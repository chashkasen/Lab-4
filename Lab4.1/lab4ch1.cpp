﻿#include <iostream>
#include <chrono>
#define N 10000
#define M 1000

class Timer
{
private:
	//         
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

struct T_List
{
	T_List* next;
	
	int n;
};

void ADD(T_List* head, int n)
{
	T_List* p = new T_List;
	p->n = n;

	p->next = head->next;
	head->next = p;
}

bool SEARCH(T_List* head, int k)
{
	T_List* p = head->next;

	while (p != nullptr)
	{
		if (p->n == k)
			return true;
		p =p->next;
	}
}

void CLEAR(T_List* head)
{
	T_List* tmp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}


int main()
{
	int k;
	T_List* head = new T_List;
	head->next = nullptr;

	std::cin >> k;

	for (int i = 0; i < N; i++)
		ADD(head, i);

	Timer t;

	for (int i = 0; i < M; i++)
		if (SEARCH(head, k))
			std::cout << "Found ";

	std::cout << "Time: " << t.elapsed();

	CLEAR(head);
	delete head;
	return 0;
}

