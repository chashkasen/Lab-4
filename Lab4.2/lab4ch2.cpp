﻿#include <iostream>
#include <chrono>
#define N 10000
#define M 1000

class Timer
{
private:
	//         
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};



void ADD(int mas[N])
{
	for (int i = 0; i < N; i++)
	{
		mas[i] = i;
	}
}

bool SEARCH(int mas[N], int k)
{
	for (int i = 0; i < N; i++)
		if (mas[i] == k)
			return true;
		
}

int main()
{
	int mas[N], k;  

	std::cin >> k;
	ADD(mas);

	Timer t;

	for (int i=0; i<M;i++)
	if (SEARCH(mas, k))
		std::cout << "Found ";

	std::cout << "Time: " << t.elapsed();

	return 0;
}

 
